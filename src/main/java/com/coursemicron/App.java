package com.coursemicron;

import com.coursemicron.db.DatabaseProvider;
import com.coursemicron.db.impl.DatabaseProviderMySQLImpl;
import com.coursemicron.exception.GlobalDBException;
import com.coursemicron.models.Blog;
import com.coursemicron.models.OptionsEnum;
import com.coursemicron.models.User;
import com.coursemicron.util.BlogPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Awesome Blogger is an app to create and publish blog
 *
 */
public class App 
{
    private static Logger log = LogManager.getLogger(App.class.getName());
    public static void main( String[] args ){
        boolean flag=true;
        while(flag){
            TextIO textIO = TextIoFactory.getTextIO();
            TextTerminal terminal = textIO.getTextTerminal();
            DatabaseProvider provider = new DatabaseProviderMySQLImpl();
            OptionsEnum option = textIO.newEnumInputReader(OptionsEnum.class)
                    .read("Chose an option:");
            switch (option){
                case REGISTER: {

                    try {
                        log.info("User selected REGISTER option");
                        terminal.printf("\nYou chose option as %s\n",option);
                        String uname = textIO.newStringInputReader()
                                .withPattern("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")
                                .read("Enter username/email");
                        String userFullName = textIO.newStringInputReader()
                                .withMinLength(5)
                                .read("Enter full name");
                        provider.createUser(new User(uname,userFullName));
                        terminal.printf("\nUser created successfully\n");
                        break;
                    } catch (GlobalDBException e) {
                        terminal.printf("\nError in creating user.\nTry again in sometime\n");
                        break;
                    }
                }

                case CREATE_BLOG:{

                    try {
                        log.info("User selected CREATE_BLOG option");
                        terminal.printf("\nYou chose option as %s\n",option);
                        String username = textIO.newStringInputReader()
                                .withMinLength(5)
                                .withPattern("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")
                                .read("Enter email address for the blog");
                        User user= null;
                        user = provider.isUserExists(username);
                        if(Objects.isNull(user)){
                            terminal.printf("\nSorry entered user name not found on system. \nPlease register before creating a blog\n");
                            break;
                        }
                        String blogTitle = textIO.newStringInputReader()
                                .read("Enter a title for the blog");
                        String btext = textIO.newStringInputReader()
                                .read("Your article text goes here. Hit enter to finish !");
                        Blog newBlog = new Blog();
                        newBlog.setUser(user);
                        newBlog.setBtitle(blogTitle);
                        newBlog.setbText(btext);
                        newBlog.setCreatedDt(new Date(System.currentTimeMillis()));
                        provider.createBlog(newBlog);

                        terminal.printf("\nYour blog got created successfully with name: %s\n",blogTitle);
                    } catch (GlobalDBException e) {
                        log.error(e);
                        terminal.printf("\nError in creating blog.\nTry again in sometime\n");
                    }
                    break;
                }

                case LIST_10_RECENT_BLOGS:{
                    try{
                        terminal.printf("\nBelow are recent 10 blogs.\n");
                        List<Blog> blogs = provider.readBlogs(10);
                        BlogPrinter.printBlog(blogs,terminal);
                    }catch (GlobalDBException e){
                        terminal.printf("\nError in fetching blogs.\nTry again in sometime\n");
                    }
                    break;
                }

                case EXIT:{
                    //textIO.dispose();
                    flag=false;
                    textIO.dispose();
                    break;
                }
            }

        }
    }
}
