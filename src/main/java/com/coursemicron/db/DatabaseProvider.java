package com.coursemicron.db;

import com.coursemicron.exception.GlobalDBException;
import com.coursemicron.models.Blog;
import com.coursemicron.models.Comment;
import com.coursemicron.models.User;

import java.sql.Connection;
import java.util.List;

public interface DatabaseProvider {
    Connection getConnection() throws GlobalDBException;
    boolean createUser(User user) throws GlobalDBException;
    List<User> readAllUsers() throws GlobalDBException;
    boolean createBlog(Blog blog) throws GlobalDBException;
    User isUserExists(String username) throws GlobalDBException;
    List<Blog> readBlogs(int howMany) throws GlobalDBException;
    User getUserById(int uid)throws GlobalDBException;
    Comment getCommentByBlogId(int bid)throws GlobalDBException;
}
