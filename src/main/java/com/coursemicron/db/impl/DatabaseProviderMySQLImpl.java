package com.coursemicron.db.impl;

import com.coursemicron.App;
import com.coursemicron.db.DatabaseProvider;
import com.coursemicron.exception.GlobalDBException;
import com.coursemicron.models.Blog;
import com.coursemicron.models.Comment;
import com.coursemicron.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseProviderMySQLImpl implements DatabaseProvider {
    private static Logger log = LogManager.getLogger(DatabaseProviderMySQLImpl.class.getName());
    private static Connection con;

    static {
        log.info("Request received to create a connection");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/awesomeblogger","root","root");
            log.info("Successfully created connection");
        } catch (ClassNotFoundException | SQLException e) {
            log.error("Error in creating db connection");
        }
    }

    /*public void init() throws GlobalDBException {
        if(con==null){
            con = getConnection();
            log.info("A new connection is created !!");
        }
    }*/

    @Override
    public Connection getConnection() throws GlobalDBException {
        log.info("Request received to create a connection");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/awesomeblogger","root","root");
            log.info("Successfully created connection");
            return con;
        } catch (ClassNotFoundException | SQLException e) {
            throw new GlobalDBException("Error in creating db connection");
        }
    }

    @Override
    public boolean createUser(User user) throws GlobalDBException {
        log.info("Request received to create user");
        //init();
        log.info(user.toString());
        try {
            String sql = String.format("insert into USER(UNAME,UFULLNAME) VALUES (\'%S\',\'%S\')",user.getUname(),user.getUserFullName());
            //sql = "insert into USER(UNAME,UFULLNAME) VALUES ('SARALA.@GMAIL.COM','SARALA JAMADAR'"
            Statement st = con.createStatement();
            int rowCount = st.executeUpdate(sql);
            log.info("User created");
            log.info("Number of rows effected in database: "+rowCount);
        } catch (SQLException e) {
            log.error(e);
            throw new GlobalDBException("Error while creating user",e);
        }finally {
            try {
                con.close();
            }catch (SQLException e){
                log.error(e);
                throw new GlobalDBException("Unable to close connection",e);
            }
        }
        return true;
    }

    @Override
    public List<User> readAllUsers() throws GlobalDBException {
        log.info("Received request to read users from db");
        // select * from user
        String sql = "select * from user"; // 2 user
        try {
            //init();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            List<User> listUser = new ArrayList<>();
            while (rs.next()){
                int uid=rs.getInt("uid");
                String uname=rs.getString("uname");
                String ufullname=rs.getString("ufullname");
                User user = new User();
                user.setuId(uid);
                user.setUname(uname);
                user.setUserFullName(ufullname);
                listUser.add(user);
            }
            log.info("Count of users in database:"+listUser.size()); // Count of users in database: 2
            return listUser;
        } catch (SQLException e) {
            log.error("Error while fetching records");
            throw new GlobalDBException(e);
        }
    }

    @Override
    public boolean createBlog(Blog blog) throws GlobalDBException {
        // insert into blog(bid,btext,uid,created_dt) value('My first blog',3,06-06-2020);
        //String sql = String.format("insert into blog(btext,uid,created_dt) values (\'%s\',%i,\'%d')",blog.getbText(),blog.getUser().getuId(),blog.getCreatedDt());
        log.info("Received request to create a blog");
        String sql = "insert into blog(btitle,btext,uid,created_dt) values(?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1,blog.getBtitle());
            ps.setString(2,blog.getbText());
            ps.setInt(3,blog.getUser().getuId());
            ps.setDate(4,blog.getCreatedDt());

            ps.executeUpdate();
            log.info("Saved blog in DB");
        } catch (SQLException e) {
            log.error("Error in saving blog");
            throw new GlobalDBException(e);
        }
        return true;
    }


    @Override
    public User isUserExists(String username) throws GlobalDBException {
        log.info("Received request to read users from db");
        // select * from user
        String sql = "select * from user where uname=?"; // 2 user
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                int uid=rs.getInt("uid");
                String uname=rs.getString("uname");
                String ufullname=rs.getString("ufullname");
                User user = new User();
                user.setuId(uid);
                user.setUname(uname);
                user.setUserFullName(ufullname);
                return user;
            }
            return null;
        } catch (SQLException e) {
            log.error("Error while fetching records");
            throw new GlobalDBException(e);
        }
    }

    @Override
    public List<Blog> readBlogs(int howMany) throws GlobalDBException {
        log.info("Received request to fetch blogs");
        String sql = "SELECT * FROM BLOG order by CREATED_DT DESC limit "+howMany;
        List<Blog> blogs = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()){
                Blog blog = new Blog();
                int bid = rs.getInt("bid");
                String btitle = rs.getString("btitle");
                String btext = rs.getString("btext");
                Date date = rs.getDate("created_dt");
                int uid = rs.getInt("uid");
                //get user info
                User user = getUserById(uid);

                //get comments for this blog

                //create blog object
                blog.setbText(btext);
                blog.setBtitle(btitle);
                blog.setCreatedDt(date);
                blog.setUser(user);
                blogs.add(blog);
            }
        } catch (SQLException e) {
            log.error("Error in fetching blogs",e);
            throw new GlobalDBException(e);
        }
        return blogs;
    }

    @Override
    public User getUserById(int uid) throws GlobalDBException {
        log.info("Received request to read users from db");
        // select * from user
        String sql = "select * from user where uid=?"; // 2 user
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1,uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                String uname=rs.getString("uname");
                String ufullname=rs.getString("ufullname");
                User user = new User();
                user.setuId(uid);
                user.setUname(uname);
                user.setUserFullName(ufullname);
                return user;
            }
        } catch (SQLException e) {
            log.error("Error while fetching records");
            throw new GlobalDBException(e);
        }
        return null;
    }

    @Override
    public Comment getCommentByBlogId(int bid) throws GlobalDBException {
        return null;
    }


}
