package com.coursemicron.exception;

import java.sql.SQLException;

public class GlobalDBException extends SQLException {
    public GlobalDBException(String reason, String SQLState, int vendorCode) {
        super(reason, SQLState, vendorCode);
    }

    public GlobalDBException(String reason, String SQLState) {
        super(reason, SQLState);
    }

    public GlobalDBException(String reason) {
        super(reason);
    }

    public GlobalDBException() {
    }

    public GlobalDBException(Throwable cause) {
        super(cause);
    }

    public GlobalDBException(String reason, Throwable cause) {
        super(reason, cause);
    }

    public GlobalDBException(String reason, String sqlState, Throwable cause) {
        super(reason, sqlState, cause);
    }

    public GlobalDBException(String reason, String sqlState, int vendorCode, Throwable cause) {
        super(reason, sqlState, vendorCode, cause);
    }
}
