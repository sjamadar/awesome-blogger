package com.coursemicron.models;

public enum OptionsEnum {
    REGISTER,
    CREATE_BLOG,
    COMMENT,
    LIST_10_RECENT_BLOGS,
    EXIT
}
