package com.coursemicron.models;

public class User {
    private int uId;
    private String uname;
    private String userFullName;

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getUname() {
        return uname.toLowerCase();
    }

    public void setUname(String uname) {
        this.uname = uname.toLowerCase();
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Override
    public String toString() {
        return "User{" +
                "uId=" + uId +
                ", uname='" + uname + '\'' +
                ", userFullName='" + userFullName + '\'' +
                '}';
    }

    public User(String uname, String userFullName) {
        this.uname = uname;
        this.userFullName = userFullName;
    }

    public User(){}

}
