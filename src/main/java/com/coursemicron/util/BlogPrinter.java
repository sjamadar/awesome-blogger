package com.coursemicron.util;

import com.coursemicron.models.Blog;
import org.beryx.textio.TextTerminal;

import java.util.List;

public class BlogPrinter {
    public static void printBlog(List<Blog> blogs,TextTerminal terminal){
        terminal.println("------THE AWESOME BLOGGER------");
        for(Blog blog:blogs){
            terminal.printf("\nAuthor email: %s",blog.getUser().getUname());
            terminal.printf("\nAuthor name: %s",blog.getUser().getUserFullName());
            terminal.printf("\nDate created: %s\n",blog.getCreatedDt().toString());
            terminal.println("---------------------------------------------");
            terminal.println("Title:\t"+blog.getBtitle());
            terminal.println("Article:\t"+blog.getbText());
            terminal.println("Comments:");
            terminal.print(blog.getComments());
            terminal.println("----------------------------------------------");

        }
    }
}
