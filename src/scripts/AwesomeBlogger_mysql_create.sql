drop database awesomeblogger;

create database awesomeblogger;

use awesomeblogger;

CREATE TABLE `user` (
	`uid` int NOT NULL AUTO_INCREMENT,
	`uname` varchar(100) NOT NULL UNIQUE,
	`ufullname` varchar(100) NOT NULL,
	PRIMARY KEY (`uid`)
);

CREATE TABLE `blog` (
	`bid` int NOT NULL AUTO_INCREMENT,
	`btitle` TEXT NOT NULL,
	`btext` TEXT NOT NULL,
	`uid` int NOT NULL,
	`created_dt` DATETIME NOT NULL,
	PRIMARY KEY (`bid`)
);

CREATE TABLE `comment` (
	`cid` int NOT NULL AUTO_INCREMENT,
	`ctext` tinytext NOT NULL,
	`bid` int NOT NULL,
	`created_dt` DATETIME NOT NULL,
	`uid` int NOT NULL,
	PRIMARY KEY (`cid`)
);

ALTER TABLE `blog` ADD CONSTRAINT `blog_fk0` FOREIGN KEY (`uid`) REFERENCES `user`(`uid`);

ALTER TABLE `comment` ADD CONSTRAINT `comment_fk0` FOREIGN KEY (`bid`) REFERENCES `blog`(`bid`);

ALTER TABLE `comment` ADD CONSTRAINT `comment_fk1` FOREIGN KEY (`uid`) REFERENCES `user`(`uid`);

